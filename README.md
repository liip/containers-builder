# Liip-standard Container building and tagging

This is a container builder "done the right way"™, aimed for the Liip Gitlab CI setup.

## Versions

Inclusion happens by pointing to this repository, with a `ref` that points to the correct branch/tag. Use:

- `v3` for guaranteed-stable latest developements
- `v2` for guaranteed-stable with only container version updates
- `main` for current development

## tl; dr: Build the repository's image

Provided you have a `Dockerfile` at the root of the repository, and want to build the repository's image, just put this in your `.gitlab-ci.yml`:

```yaml
---
include:
  - project: "liipops/containers-builder"
    file: "/tasks.yml"
    ref: v3

Build repository OCI:
  extends: .build-definition
  variables:
    BUILD_IMAGE_NAME: ""
```

## tl; dr: Build a named image

With a `Dockerfile` at a specific path, and a custom name, that would be your `.gitlab-ci.yml`:

```yaml
---
include:
  - project: "liipops/containers-builder"
    file: "/tasks.yml"
    ref: v3

Build Babylon Tower OCI:
  extends: .build-definition
  variables:
    BUILD_IMAGE_NAME: babylon
    BUILD_PATH: ./towers/babylon
```

## tl; dr: Building images for multiple architectures

Put this in your `.gitlab-ci.yml`:

```yaml
---
include:
  - project: "liipops/containers-builder"
    file: "/tasks.yml"
    ref: v3

variables:
  BUILD_IMAGE_NAME: "test"

stages:
  - build
  - tag

Build image:
  stage: build
  extends:
    - .build-definition
    - .docker-parallel-on-all-archs
  variables:
    BUILD_IMAGE_TAG_POSTFIX: "-${DOCKER_CPU_ARCH}"

Tag images as multiarch manifest:
  stage: tag
  extends:
    - .manifest-create-definition
  variables:
    BUILD_IMAGE_SOURCE_TAGS: "${CI_COMMIT_REF_SLUG}-amd64 ${CI_COMMIT_REF_SLUG}-arm64v8"
  needs:
    - "Build image"
```

For a more complete example, check this repo's [`.gitlab-ci.yml`](./.gitlab-ci.yml).

## tl; dr: Allowing builds outside Switzerland

Make sure to have `RUNNER_TAG_POSTFIX` set in your `.gitlab-ci.yml`:

```yaml
variables:
  RUNNER_TAG_POSTFIX: "-nonswiss-ok"
```

## A set of targets

The `containers-builder` repository is a collection of ready-to-reuse Gitlab CI targets, aimed at being used as largely as possible across Liip.

The targets all start with a dot (`.`) to not be executed by default. One needs to use an `extends:` to make use of it. All parts (from `variables` to `before_script` can be then be overriden / extended at will.

### High-level

#### `.build-definition` - Build a Docker image

This is the main target one will likely use: build a Docker image and push it to the repository's registry.

```yaml
.build-definition: &build-definition
  variables:
    # The image name, defaults to repository name
    BUILD_IMAGE_NAME: ${CI_PROJECT_DIR}
    # Path where to situate the build, the root of relative paths
    BUILD_PATH: .
    # Allows alternate Dockerfile names
    DOCKERFILE: ./Dockerfile
    # The image tag, defaults to the branch name
    BUILD_IMAGE_TAG: ${CI_COMMIT_REF_SLUG}
    # A tag postfix, defaults to empty
    BUILD_IMAGE_TAG_POSTFIX: ""
    # Additional build arguments
    BUILD_ARGS: ""
    # Kaniko cache TTL
    CACHE_TTL: 24h
```

#### `.tag-copy-definition` - Copy/rename a remote tag

Allows to copy a tag from one name (e.g. `main-untested`) to another (`main`).

```yaml
.tag-copy-definition: &tag-copy-definition
  variables:
    # The image name, defaults to repository name
    BUILD_IMAGE_NAME: ${CI_PROJECT_DIR}
    # In case it's different
    IMAGE_NAME_FROM: ""
    # In case it's different
    IMAGE_NAME_TO: ""
    # The original image tag, defaults to the branch name
    IMAGE_TAG_FROM: ${CI_COMMIT_REF_SLUG}
    # The original image tag, defaults to the branch name
    IMAGE_TAG_TO: "${CI_COMMIT_REF_SLUG}-released"
```

#### `.tag-force-docker-format-definition` - Pull/push a tag to enforce docker manifest format

Allows to force a tag to be a docker manifest tag, and not an OCI tag

```yaml
.tag-force-docker-format-definition:
  variables:
    # The image name, defaults to repository name
    BUILD_IMAGE_NAME: ${CI_PROJECT_DIR}
    # The image that was built as oci manifest, that needs to be re-pushed as docker manifest
    IMAGE_TAG: ${CI_COMMIT_REF_SLUG}
```

#### `.delete-tags-definition` - Remove/delete a remote tag

Allows the removal of a tag on the registry. Beware not to remove tags being the only available targets for a manifest; it will break the manifest!

```yaml
.delete-tags-definition:
  variables:
    # The image name, defaults to repository name
    BUILD_IMAGE_NAME: ${CI_PROJECT_DIR}
    # Space-separated list of tags to delete from repository
    IMAGE_TAGS: ""
    # If set, will delete all matching prefixes
    IMAGE_TAG_PREFIXES: ""
    # Space-separated list of tags to keep from repository (if IMAGE_TAG_PREFIXES is too broad for you)
    IMAGE_TAGS_TO_KEEP: ""
    # Space-separated list of image names to delete from repository. If empty, defaults to standard image name.
    IMAGE_NAMES: ""
```

#### `.manifest-create-definition` - Create a docker manifest

This is especially useful to build manifests "merging" multiple different-architectures' docker tags.

```yaml
.manifest-create-definition: &manifest-create-definition
  variables:
    # The image name, defaults to repository name
    BUILD_IMAGE_NAME: ${CI_PROJECT_DIR}
    # The final image tag, defaults to the branch name
    BUILD_IMAGE_FINAL_TAG: ${CI_COMMIT_REF_SLUG}
    # Space-separated list of image tags we want in the final tag
    BUILD_IMAGE_SOURCE_TAGS: ""
```

#### `.helm-chart-builder-definition` - Build a helm chart

This is useful to build and upload helm Charts to the Gitlab repositories.

```yaml
.helm-chart-builder-definition:
  variables:
    # Name of the Helm chart; has to be its name too, relative to ${HELM_CHART_PATH}. It can be empty if the repository _is_ the chart directory
    HELM_CHART_NAME: my-custom-chart
    # git tag prefix that the tags need to respect to complete these builds. If unset, will not check.
    # HELM_CHART_TAG_PREFIX: "my-custom-chart/"
    # Channel ('stable' usually), but let's default to always push to the 'main' channel
    HELM_CHART_CHANNEL: "${CI_DEFAULT_BRANCH}"
    # Path where to situate the build, the root of relative paths
    HELM_CHART_PATH: .
```

### Low-level

Some of these tasks have deeper levels of definitions, for lower-level common things.

- `.docker-in-docker` - Allows to use the normal `docker` client, with a running `docker` daemon.
- `.crane-definition` - Allows to use the `crane` repository client
